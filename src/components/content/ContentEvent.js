const initialState = {
    courses: []
};

const ContentEvent = (state = initialState, action) => {
    switch(action.type) {
        case "ADD_COURSES": {
            console.log(`Reducer Đã nhận được dữ liệu: ${action.payload.courses}`);
            return {
                courses: [...action.payload.courses]
            };
        }
        default: {
            return state;
        };
    };
};

export default ContentEvent;