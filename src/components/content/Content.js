import Banner from "./banner/Banner.js";
import CoursesZone from "./slide/CoursesZone.js";
import { useDispatch } from "react-redux";
import { useEffect, useState } from "react";

const Content = () => {
    const dispatch = useDispatch();
    const [vCheckGetedDataFromApi, setVCheckGetedDataFromApi] = useState(false);
    // Call API    
    const getAPI = async (paramURL, paramRequest) => {
        try {
            const getApiResult = await fetch(paramURL, paramRequest);
            const getApiJsonData = await getApiResult.json();
            const allCourses = await getApiJsonData.courses;
            setVCheckGetedDataFromApi(true);
            dispatchAllCourseData(allCourses);
        } catch (err) {
            console.log(`Lỗi khi call API (GET)`);
            console.log(err);
            throw new Error();
        } finally { 
            console.log(`get API Data Done!`);
        };
    };

    // Get full course API handler
    const callAPIToGetFullCourses = () => {
        const myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        const getFullCoursesUrl = 'http://localhost:8000/api/courses';
        const getFullCoursesRequestOption = {
            method: "GET",
            redirect: "follow",
            headers: myHeaders,
            mode: "cors"
        };
        getAPI(getFullCoursesUrl, getFullCoursesRequestOption);
    };

    const dispatchAllCourseData = (paramAllCourses) => {
        dispatch({
            type: "ADD_COURSES",
            payload: {
                courses: paramAllCourses
            }
        })
    }

    useEffect(() => {
        callAPIToGetFullCourses();
    }, [])

    return (
        <>
            <Banner/> {/* Vùng hiện Banner, Slide Show*/}
            <CoursesZone checkGetedDataProp={vCheckGetedDataFromApi} /> {/* Vùng hiện các khóa học */}
        </>
    );
};

export default Content;