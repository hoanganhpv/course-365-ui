import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';
import { Row, Col } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBookmark, faClock, faL } from "@fortawesome/free-solid-svg-icons";

const CourseCard = (props) => {

    const datagetted = props.recommendDataProp;

    return (
        datagetted.map((course, index) => {
            return <Col key={index} xl={3}>
                <Card sx={{ maxWidth: 345 }}>
                    <CardActionArea>
                        <CardMedia
                            component="img"
                            height="140"
                            image={course.coverImage}
                            alt="green iguana"
                        />
                        <CardContent className='row h-100 justify-content-between' style={{ minHeight: "165px" }}>
                            <Typography className='col-12' style={{ fontSize: "15px", color: "#1D8CFF", fontWeight: "600" }} gutterBottom variant="h5" component="div">
                                {course.courseName}
                            </Typography>
                            <Typography className='col-12' variant="body2" color="text.secondary">
                                <FontAwesomeIcon icon={faClock} /><span style={{ marginLeft: "10px" }}>{course.duration}</span><span style={{ marginLeft: "10px" }}>{course.level}</span>
                            </Typography>
                            <Typography className='mt-2 col-12' variant="body2" color="text.secondary">
                                <span style={{ fontWeight: "bold" }}>${course.discountPrice}</span> <span style={{ textDecoration: "line-through" }}>${course.price}</span>
                            </Typography>
                        </CardContent>
                        <Row className='d-flex justify-content-around py-2' style={{ backgroundColor: "#F7F7F7" }}>
                            <Col xl={9}>
                                <img style={{ borderRadius: "50%" }} width="20%" src={course.teacherPhoto}></img>
                                <span style={{ fontSize: "15px", marginLeft: "15px", fontWeight: "400" }}>{course.teacherName}</span>
                            </Col>
                            <Col xl={1} style={{ marginRight: "10px" }}>
                                <FontAwesomeIcon style={{ marginTop: "13px" }} icon={faBookmark} />
                            </Col>
                        </Row>
                    </CardActionArea>
                </Card>
            </Col>
        })
    );
};

export default CourseCard;