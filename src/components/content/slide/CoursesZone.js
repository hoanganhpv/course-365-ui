import PopularCourses from "./popularCourses/PopularCourses.js";
import RecommendedCourses from "./recommendedCourses/RecommendedCourses.js";
import TrendingCourses from "./trendingCourses/TrendingCourses.js";
import { Container } from "reactstrap";

const Slide = (props) => {
    const vCheckGetedDataFromApi = props.checkGetedDataProp;
    return (
        <Container className="mt-5 w-75">
            <RecommendedCourses checkGetedDataProp={vCheckGetedDataFromApi}/>
            <PopularCourses checkGetedDataProp={vCheckGetedDataFromApi}/>
            <TrendingCourses checkGetedDataProp={vCheckGetedDataFromApi}/>
        </Container>
    );
};

export default Slide;