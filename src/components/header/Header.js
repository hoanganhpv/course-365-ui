// ReactStrap
import { Col, Row } from "reactstrap";

// MUI library
import React from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSortDown } from "@fortawesome/free-solid-svg-icons";

// components
// Logo
import logo from '../../assets/images/Ionic_logo.png';

const Header = () => {
    return (
        <>
            <AppBar style={{ backgroundColor: "#F8F9FA" }} position="static">

                <Row className="d-flex justify-content-between">
                    {/* LEFT navbar */}
                    <Col xl={4} className="d-flex justify-content-center">
                        <Toolbar className="d-flex justify-content-around">
                            {/* Logo */}
                            <Typography component="div">
                                <img src={logo}></img>
                            </Typography>
                            {/* Nút điều hướng */}
                            <Button style={{ backgroundColor: "#F8F9FA", color: "#4A4370", width: "30px", textTransform: "none", border: "none", boxShadow: "none", fontWeight: "500" }} variant="contained">Home </Button>
                            <Button style={{ backgroundColor: "#F8F9FA", color: "#D0C7C1", width: "130px", textTransform: "none", border: "none", boxShadow: "none" }} variant="contained">Browse&nbsp;course&nbsp;&nbsp;<FontAwesomeIcon style={{ fontSize: "18px", lineHeight: "18px" }} className="my-auto pt-0 pb-2" icon={faSortDown} /></Button>
                            <Button style={{ backgroundColor: "#F8F9FA", color: "#D0C7C1", width: "100px", textTransform: "none", border: "none", boxShadow: "none" }} variant="contained">About&nbsp;Us&nbsp;&nbsp;<FontAwesomeIcon style={{ fontSize: "18px", lineHeight: "18px" }} className="my-auto pt-0 pb-2" icon={faSortDown} /></Button>
                        </Toolbar>
                    </Col>
                    {/* RIGHT navbar */}
                    <Col xl={3} className="d-flex justify-content-center">
                        <TextField label="Search Courses" style={{ margin: "12px 0px" }} inputProps={{ style: { height: '7px', fontSize: '17px' } }} rows={1} />
                        <Button style={{padding: "2px 20px",  marginTop: "10px", marginBottom: "11px", marginLeft: "10px", width: "17px", textTransform: "none", color: "#63A2FC", border: ".2px solid #63A2FC" }}>Search</Button>
                    </Col>
                </Row>
            </AppBar>
        </>
    );
};

export default Header;