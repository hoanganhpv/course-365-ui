import { createStore, combineReducers } from "redux";
import HeaderEvent from "./components/header/headerEvent.js";
import ContentEvent from "./components/content/ContentEvent.js";

const appReducer = combineReducers({
    headerReducer: HeaderEvent,
    contentReducer: ContentEvent
});

const store = createStore(appReducer);

export default store;