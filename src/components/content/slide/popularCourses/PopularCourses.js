import { useSelector } from "react-redux";
import CourseCard from "../item/CourseCard.js";
import { Row } from "reactstrap";
import { useEffect, useState } from 'react';

const PopularCourses = (props) => {
    const vCheckGetedDataFromApi = props.checkGetedDataProp;
    const { courses } = useSelector(reducerData => reducerData.contentReducer);
    const [recommendCourse, setRecommendCourse] = useState([]);
    useEffect(() => {
        if (vCheckGetedDataFromApi === true) {
            let vRecommends = [];
            let vCheck = false;
            let vIndex = 0;
            let vNumber = 0;
            while (vCheck === false && vIndex < courses.length) {
                if (courses[vIndex].isPopolar === true && courses[vIndex].isTrending === false) {
                    vRecommends.push(courses[vIndex]);
                    ++vNumber;
                    ++vIndex;
                    if (vNumber === 4) {
                        vCheck = true;
                    };
                } else {
                    ++vIndex;
                };
            };
            setRecommendCourse(vRecommends);
        }
    }, [vCheckGetedDataFromApi]);
    return (
        <>
            <div className="mt-5">
                <h2 className="mb-4">Most Popular</h2>
                <Row>
                    <CourseCard recommendDataProp={recommendCourse} />
                </Row>
            </div>

        </>
    );
};

export default PopularCourses;









