import { Row, Col, Container } from "reactstrap";

import * as React from 'react';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';

import imageBanner from "../../../assets/images/ionic-207965.png";

const Banner = () => {
    return (
        <>
            {/* Main */}
            <Row style={{backgroundColor: "#16A3B8"}}>
                <Col className="d-flex justify-content-center align-items-center" xl={6} md={6} sm={6} xs={6}>
                    <Row className="xl-12 sm-12 md-12 xs-12">
                            <Container style={{marginLeft: "80px"}}>
                                <Row className="d-flex justify-content-center">
                                    <Col xl={8} md={8} sm={8} xs={8}>
                                    <h1 style={{color: "#fff"}}>Welcome to Ionic Course365 Learning Center</h1>
                                    </Col>
                                </Row>
                                <Row className="d-flex justify-content-center">
                                    <Col xl={8} md={8} sm={8} xs={8}>
                                    <p style={{color: "#82CED6", fontSize: "17px"}}>Ionic Course365 is an online learing and teaching marketplace with over 5000 courses and 1 million students and expertly crafted courses, designed for the modern students and entrepreneur.</p>
                                    </Col>
                                </Row>
                                <Row className="d-flex justify-content-center">
                                    <Col xl={8} md={8} sm={8} xs={8}>
                                    <Button className="py-2" style={{backgroundColor: "#FEC007", color: "#000", fontWeight: "500", textTransform: "none"}} variant="contained">Browse Courses</Button>
                                    <Button className="py-2 mx-3" style={{backgroundColor: "#FEFEFF", width: "40%", color: "#000", fontWeight: "500", textTransform: "none"}} variant="contained">Become an Instructor</Button>
                                    </Col>
                                </Row>
                            </Container>
                    </Row>

                </Col>
                
                <Col xl={6} md={6} sm={6} xs={6}>
                    <img src={imageBanner}></img>
                </Col>
            </Row>
        </>
    );
};

export default Banner;