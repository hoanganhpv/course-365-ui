import React from "react";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import { Col, Row } from "reactstrap";

const Footer = () => {
    return (
        <footer
            sx={{
                backgroundColor: (theme) => theme.palette.primary.main,
                padding: (theme) => theme.spacing(2),
                color: (theme) => theme.palette.common.white,
                marginTop: "auto",
            }}
        >
            <Container maxWidth="xl" className="mt-5">
                <Row style={{ width: "100%" }} className="d-flex justify-content-around">
                    <Col xl={6} xs={6} md={6} lg={6} item className="d-flex justify-content-center">
                        <Typography variant="body1" align="center">
                            © 2021 Ioconic Course365. All rights reserved.
                        </Typography>
                    </Col>
                    <Col xl={3} xs={3} md={3} lg={3} item>
                        <Row className="d-flex justify-content-around">
                            <Col style={{ color: "#7EAFFE", fontWeight: "500" }} xl={2} xs={2} md={2} lg={2}>Privacy</Col>
                            <Col style={{ color: "#7EAFFE", fontWeight: "500" }} xl={2} xs={2} md={2} lg={2}>Terms</Col>
                            <Col style={{ color: "#7EAFFE", fontWeight: "500" }} xl={2} xs={2} md={2} lg={2}>Feedback</Col>
                            <Col style={{ color: "#7EAFFE", fontWeight: "500" }} xl={2} xs={2} md={2} lg={2}>Support</Col>
                        </Row>
                    </Col>
                </Row>
            </Container>
        </footer>
    );
};

export default Footer;