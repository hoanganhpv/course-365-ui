import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './components/header/Header.js';
import Footer from './components/footer/Footer.js';
import "./App.css";

import Content from './components/content/Content.js';

function App() {
  return (
    <div>
      <Header />
      <Content />
      <Footer />
    </div>
  );
}

export default App;
